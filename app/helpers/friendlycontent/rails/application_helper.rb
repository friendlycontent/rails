module Friendlycontent
  module Rails
    module ApplicationHelper
      def fcblock(path, options={})
        source_name = options[:source] || Friendlycontent::Rails.config.default_content_source_name
        source_info = ::Friendlycontent::Rails.config.get_source_info(source_name)
        return Friendlycontent::Rails::Renderer.new(self).render_block( path, source_info, options)
      end
    end
  end
end
