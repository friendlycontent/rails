require 'active_support/concern'

module Friendlycontent
  module Rails
    module RenderController
      extend ActiveSupport::Concern

      included do
        # method to add to controller
        def render_remote(path, options={})
          source_name = options[:source] || Friendlycontent::Rails.config.default_templates_source_name
          source_info = ::Friendlycontent::Rails.config.get_source_info(source_name)

          if source_info.is_local?
            raise 'Local source not supported. Use render instead'
          end

          ::Friendlycontent::Rails::Renderer.new(self).render_block( path, source_info, options)
        end
      end
    end
  end
end