module Friendlycontent
  module Rails
    class WebhooksController < ActionController::Base
      #protect_from_forgery with: :exception
      skip_before_action :verify_authenticity_token

      def update
        res = {}

        # input
        toDownload = (params[:update] || 0).to_i == 1

        data = params.permit!

        source_name = params[:source].to_s

        files_modified = []
        commits = params[:webhook][:commits] rescue []
        commits.each do |commit|
          files_modified.push(*commit[:added])
          files_modified.push(*commit[:modified])
        end

        files_modified.uniq!

        #logger.debug "webhook. changed files: "
        #logger.debug files_modified.inspect

        source_info = ::Friendlycontent::Rails.config.get_source_info(source_name)
        unless source_info
          res[:res] = 0
          (render json: res and return)
        end

        # expire
        virtual_paths = ::Friendlycontent::Rails::ContentManager.get_virtual_paths source_info, files_modified
        ::Friendlycontent::Rails::ContentManager.expire source_info, virtual_paths

        #::Friendlycontent::Rails::ContentManager.expire_source_files(source_name, files_modified)

        # download now
        if toDownload
          ::Friendlycontent::Rails::ContentManager.download_files(source_name, virtual_paths)
        end

        res[:res] = 1

        render json: res
      end
    end
  end
end
