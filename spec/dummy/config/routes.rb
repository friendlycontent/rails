Rails.application.routes.draw do

  get '/', to: 'home#index'

  mount Friendlycontent::Rails::Engine => "/friendlycontent"

end
