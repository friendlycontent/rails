Friendlycontent::Rails::Engine.routes.draw do
  match "/webhook" => 'webhooks#update', via: [:get, :post]
end
