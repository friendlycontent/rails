# Friendlycontent::Rails

## Overview

Work with remote content in Rails application.

Get content from remote sources like Github, Gitlab, FriendlyContent.io.

Supported contents:
* template files - HTML, ERB with html (.erb.html), Haml (.html.haml)
* images
* any text file

Supported remote storages:
* Github - use gem friendlycontent-source-github
* Gitlab - use gem friendlycontent-source-gitlab
* SSH storage - use gem friendlycontent-source-ssh


For more formats of content and storages use service https://friendlycontent.io.
Friendlycontent.io can be used to manage and serve content for your application.
Service allows you to transform files from  one format to another.

Examples:
* Markdown file to HTML
    

## Usage in Rails app

Content:
* templates (views)
* blocks (partial views)
* images
* any file which can be stored in Rails app



## Installation

Gemfile
```ruby
gem 'friendlycontent-rails'
```



## Configuration


* `config/initializers/friendlycontent.rb`

Specify sources.

```ruby
Friendlycontent::Rails.configure do |c|
  c.default_templates_source_name = "templates_remote" # default 'local'
  c.default_content_source_name = "remote_github"

  c.content_sources = {
      "local" => {
          type: 'local',
          path: 'app/views'
      },
      "templates_remote" => {
          type: 'remote',
          api_url: "https://github.com/democorp/prj1/site/templates/",
          auth_user: "myuser",
          auth_password: "password",
          class_source: "::Friendlycontent::Source::Github"
      },
      "remote_github" => {
          type: 'remote',
          api_url: "https://github.com/democorp/prj1/site/",
          auth_user: "myuser",
          auth_password: "password",
          class_source: "::Friendlycontent::Source::Github"
      },
  }
end
```

Note. Additional gems should be used depending on source type.
See Source storages.

* or use config in YAML `config/friendlycontent.yml`


## Setup helpers

* include helpers into controller

```ruby
class ApplicationController < ActionController::Base
  include ::Friendlycontent::Rails::RenderController
  
  
end

```

It will make function `render_remote` work in your controller.

* Helpers for view
Helpers are automatically included after gem is required.
You can use `block` in your view to render content from remote source.
 
 
## Usage


### Render template (view)

Render template (view) from remote source.

Render from controller:
```ruby
class Mycontroller < ApplicationController

    def myaction
      ...
      
      # render from default source
      render_remote 'product/show'
    end
end
```

* A new method `render_remote` is automatically added to every Controller of Rails app



* render file from remote source
```ruby
# home_controller.rb
 
def show
    ..
    #
    render_remote 'page', source: 'templates_remote'
 
end
```

Specify source with name `templates_remote` in config.

In this example, source `templates_remote` is a storage on Github.

```ruby
# config/initializers/friendlycontent.rb
Friendlycontent::Rails.configure do |c|
  c.default_templates_source_name = "templates_remote" # default 'local'
  c.default_content_source_name = "templates_remote"

  c.content_sources = {
      "local" => {
          type: 'local',
          path: 'app/views'
      },
      "templates_remote" => {
          type: 'remote',
          api_url: "https://github.com/democorp/prj1/site/",
          auth_user: "myuser",
          auth_password: "password",
          class_source: "::Friendlycontent::Source::Github"
      }      
  }
end
```

Note. Include gem `friendly-content-source-github` for sources on Github.
See all available storages - Remote sources.

* `render_remote` can be used to render local views as usual Rails' views.

* render local view 

```ruby
# home_controller.rb
 
def show
    ..
    # renders local file app/views/show.html.haml
    render_remote 'show', source: 'local' 
end
```

with source config for local directory `app/views`

```ruby
# config/initializers/friendlycontent.rb

Friendlycontent::Rails.configure do |c|
  c.content_sources = {
      "local" => {
          type: 'local',
          path: 'app/views'
      },
      # other sources
      "templates_remote" => {
          ..
      }
  }
end
```

* render local file depending on context

```ruby
# app/controllers/home_controller.rb
 
def show
  # renders app/views/home/notfound.html.haml
  render_remote 'notfound' 
  
  # renders app/views/shared/notfound.html.haml
  render_remote 'shared/notfound' 

  # renders app/views/block1.html.haml
  render_remote 'block1' 

end
```


### Render block

Render block (partial view).

Note that file can be without starting underscore in filename.
(`home/block1.html.haml` for FriendlyContent vs `home/_block1.html.haml` for Rails).


* in view:
```haml
=block 'path/name', {options}
```

In case of local source it will render files with additional options.

Examples.
* 
```haml
# app/views/home/index.html

=block 'block1'
```
It will render file `app/views/home/block1.html.haml`.
Note that file name is without underscore.


* Pass variables to block
```haml
=block 'block1', {options}
```

Example:
```haml
=block 'block', p1: 100, p2: 'value2'
```

### Images
Use CDN to point to FC.io api endpoint


### Get data of any format



      

## Caching

## Update content

### Automatic updates with Webhooks

### Manual update



# Config

```ruby
Friendlycontent::Rails.configure do |c|
  c.option = value
  ..
end    
```

Options:
* default_templates_source_name - default source used to render templates (views).
Default is 'local'.
* default_content_source_name - default source used for rendering block (`block`)
* caching - settings for caching
* content_sources - list of sources:
```ruby
{
  source_name: {source_properties},
  ..
}

```

# Advanced

* multilingual content

Files stored on remote source:

Use FriendlyContent.io
 