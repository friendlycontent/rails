module Friendlycontent
  module ContentBlock
    class Factory
      def self.create(source_info, path, options={})
        block_options = options.clone

        cls = nil
        if source_info.type=='local'
          cls = Local
        elsif source_info.type=='remote'
          cls = Remote
        else
          raise "Source type #{source_info.type} not supported"
        end

        obj = cls.new( path, source_info, block_options)
        obj
      end
    end
  end
end

