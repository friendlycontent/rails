module Friendlycontent
  module ContentBlock
    class Remote < Base
      attr_accessor :contents
      attr_accessor :remote_source_file_data

      def initialize( _path, _source_info, _options={})
        super _path, _source_info, _options

        init_from_cache
      end

      ##
      def get_remote_url(opts={})
        source_info.get_property(:server_url) + source_info.get_property(:server_path, '')
            path+"?"+opts.to_query
      end

      ##
      def get_file
        metadata_cache = get_metadata_from_cache
        expired = metadata_cache.nil?

        # download if needed
        if expired || local_file.nil? || !local_file.exists?
          download_file
        end

        contents
      end

      def get_file_info
        download_file_info
      end

      def get_metadata
        data = get_metadata_from_cache

        if data.nil?
          download_file_info
          save_metadata
          data = get_metadata_from_cache
        end

        data
      end

      ### remote content

      # force download
      def download
        download_file
      end

      def download_file
        source = source_info.get_source_class
        @remote_source_file_data = source.get_file path, source_info.data, options

        return nil if @remote_source_file_data.nil?

        @contents = remote_source_file_data['content']

        # update local file
        #local_file_basepath = build_local_file_basepath(remote_source_file_data['path'])
        #local_file.update_basepath local_file_basepath

        # get file name from remote source
        metadata = build_metadata
        local_file.update_basepath metadata[:local_basepath]
        local_file.save_contents @contents

        # save metadata
        save_metadata_to_file metadata

        @contents
      end

      def download_file_info
        @remote_source_file_data = source.get_file path, options
      end

      def get_contents
        # check cache
        if local_file.exists?
          return local_file.get_contents
        end

        # download
        download_file

        # return contents
        self.contents
      end

      def expire!
        metadata = get_metadata_from_cache
        metadata ||= {}
        metadata[:expired] = 0
        save_metadata_to_file metadata

        true
      end

      ### local file
=begin
      def build_local_file_basepath(_source_path)
        if local_file_storage_type==STORAGE_TYPE_REMOTE_VIEWS
          return _source_path
        elsif local_file_storage_type==STORAGE_TYPE_REMOTE_BASIC
          s = build_hashstring(path+"--"+options.to_s)
          return s
        end

        raise 'storage type not supported'
      end
=end

      ### metadata
      def init_from_cache
        # init basepath from cache
        file_meta = metadata_cache_file_path
        if File.exists? file_meta
          data = YAML.load_file file_meta

          if data
            v_local_basepath = data[:local_basepath]

            if v_local_basepath
              local_file.update_basepath v_local_basepath
            end
          end
        end
      end

      def metadata_cache_file_path
        h = build_hashstring(path+"--"+options.to_s)
        File.join(::Rails.root, Friendlycontent::Rails.config.cache_tmp_dir, source_info.name, h+".meta.yml")
      end

      def save_metadata
        save_metadata_to_file build_metadata
      end

      def save_metadata_to_file(data)
        fpath = metadata_cache_file_path
        FileUtils.mkdir_p(File.dirname(fpath))
        File.open(fpath,"w+") do |f|
          f.write data.to_yaml
        end
      end

      def get_metadata_from_cache
        file_meta = metadata_cache_file_path
        if !File.exists? file_meta
          return nil
        end

        metadata = YAML.load_file file_meta

        # check expiration
        now = Time.now.utc.to_i
        if metadata[:expired].nil? || metadata[:expired] < now
          metadata = nil
        end

        metadata
      end

      def build_metadata
        source_path = remote_source_file_data['path']
        server_path = source_info.get_property(:server_path, '')
        local_basepath = source_path.gsub /^\/#{server_path}/, ''
        local_path = File.join(source_info.get_local_path, local_basepath)
        source_fullpath = File.join(server_path, source_path)
        {
            virtual_path: path,
            options: options.to_json,
            source_name: source_info.name,
            source_path: remote_source_file_data['path'],
            source_fullpath: source_fullpath,
            source_url: remote_source_file_data['source_url'],
            local_basepath: local_basepath,
            local_path: local_path,
            expired: Time.now.utc.to_i + source_info.cache_ttl
        }
      end

      ### remote file
      def remote_file_url
        remote_source_file_data['source_url']
      end

      ### utils
      def build_hashstring(s)
        require 'digest/sha1'
        Digest::SHA1.hexdigest s
      end

    end
  end
end