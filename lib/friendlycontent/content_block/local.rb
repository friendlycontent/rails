module Friendlycontent
  module ContentBlock
    class Local < Base
      def initialize( _path, _source_info, _options={})
        super _path, _source_info, _options
      end

      def get_file
        # init if needed
        if local_file.nil? || !local_file.exists?
          init_local_file
        end

        if !local_file.exists?
          raise 'file not found'
        end
      end

      def get_file_info
        # do nothing
      end

      def get_contents
        if local_file.exists?
          return local_file.get_contents
        end

        #raise 'not found'
        nil
      end

      ###
      def init_local_file
        local_file_basepath = resolve_local_file_path

        if local_file_basepath
          local_file.update_basepath local_file_basepath
        end
      end

      def resolve_local_file_path
        source_basedir = source_info.get_property(:server_path) || 'app/views'
        file_path = nil

        name = path
        # TODO: move out from here
        #lang = opts[:lang] || ''
        lang = ''
        current_dir = ''

        # try. HTML file in the current folder
        #f = File.join(Dir.pwd, dir_base, d, name+'.html')

        # more trials
        # extensions
        extensions = ['', '.html.haml', '.html.erb', '.html']

        #
        names = []

        #
        parts = name.split /\//

        parts[-1] = '_'+parts[-1]
        name2 = parts.join('/')

        #
        names << [current_dir, name] if current_dir != ''
        names << ["", name]

        names << [current_dir, name2] if current_dir != ''
        names << ["", name2]

        f = nil

        # try 2
        names.each do |p|
          extensions.each do |ext|
            f = nil

            # with lang
            if (lang || '')!=''
              cand_basepath = File.join(p[0], p[1]+'.'+lang.to_s+ext)
              ff = File.join(Dir.pwd, source_basedir, cand_basepath)
              if File.exists? ff
                file_path = cand_basepath
                f = ff
                break
              end
            end

            # without lang
            cand_basepath = File.join(p[0], p[1]+ext)
            ff = File.join(Dir.pwd, source_basedir, cand_basepath)
            if File.exists? ff
              file_path = cand_basepath
              f = ff
            end
          end

          #
          break if f && File.exists?(f)
        end

        return file_path
      end
    end
  end
end

