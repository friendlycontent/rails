module Friendlycontent
  module ContentBlock
    class Base
      attr_accessor :source_info, :path, :options
      attr_accessor :local_file

      def initialize(_path, _source_info, _options={})
        @source_info = _source_info
        @path = _path
        @options = _options

        # init local file
        @local_file = LocalFile.new(self)
      end

      # should be implemented in a derived class
      def get_file
        raise 'not implemented'
      end

      def get_file_info
        raise 'not implemented'
      end

      def get_contents
        raise 'not implemented'
      end

      def download
        raise 'not implemented'
      end

      def expire!
        raise 'not implemented'
      end

      ## properties
      def is_local?
        source_info.is_local?
      end
    end
  end
end