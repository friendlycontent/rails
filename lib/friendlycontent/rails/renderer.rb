module Friendlycontent
  module Rails
    class Renderer
      attr_accessor :context, :options

      def initialize(_ctx, opts={})
        @context = _ctx
        @options = opts
      end

      def render_block(path, source_info, _options={})
        block_options = build_options _options
        virtual_path = build_block_path path, options
        content_block = ::Friendlycontent::ContentBlock::Factory.create source_info, virtual_path, block_options
        return render_content_block content_block
      end

      ### main method to render content
      def render_content_block(content_block)
        # init file
        content_block.get_file

        f = content_block.local_file.path

        # render from file
        if File.exists? f
          return @context.render file: f, locals: content_block.options
        end

        # default render
        @context.render f, content_block.options
      end


      ### helpers

      def build_block_path(virtual_path, context, options={})
        block_path = virtual_path

        # respect current dir
        if virtual_path =~ /^[^\/]+$/
          current_template_dir = context_template_dir
          if current_template_dir!='' && current_template_dir!='.'
            block_path = File.join current_template_dir, virtual_path
          end
        end

        block_path
      end

      def context_template_dir
        if context.is_a? ActionController::Base
          current_template_dir = context.lookup_context.prefixes[0]
        else
          context_template_virtual_path = context.instance_variable_get('@virtual_path')
          current_template_dir = File.dirname context_template_virtual_path
        end

        current_template_dir
      end

      def build_options(block_options={})
        options = block_options.dup
        [:source].each { |key| options.delete(key) }

        # TODO: get options from pagedata
        #if pagedata
        #  options[:lang] ||= pagedata.lang
        #end


        options
      end
    end
  end
end
