module Friendlycontent
  module Rails
    class Configuration
      TTL_DEFAULT = 365*24*60*60
      attr_accessor :yaml_config

      # remote sources
      attr_accessor :content_sources,
                    :default_content_source_name,
                    :default_templates_source_name,
                    :cache

      def initialize
        load_config
      end

      def load_config
        begin
          @yaml_config = ::Rails.application.config_for(:friendlycontent)
        rescue => e
        end

        @yaml_config ||= {}

        # content sources
        #@content_sources ||= @yaml_config['content_sources'] || {}
        #@default_content_source_name ||= @yaml_config['default_content_source'] || ''
        #@default_templates_source_name ||= @yaml_config['default_templates_source_name'] || 'local'
        # caching = @yaml_config['caching'] || {}
      end

      def template_content_sources
        return @template_content_sources unless @template_content_sources.nil?

        @template_content_sources = @content_sources.select { |k, r| (r[:use_templates] || false) }.keys
      end

      def get_source_info(source_name)
        source_data = content_sources[source_name]
        return nil if source_data.nil?
        ::Friendlycontent::Rails::SourceInfo.new source_name, source_data
      end

      def cache
        @cache ||= {}
        @cache
      end

      def cache_ttl
        cache[:ttl] || TTL_DEFAULT
      end

      def cache_tmp_dir
        cache[:temp_dir] || "tmp/remote_content_cache"
      end
    end
  end
end