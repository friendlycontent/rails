module Friendlycontent
  module Rails
    class ContentManager
      # files - array of file names on the remote source
      # example: ['home/index.html.haml', ..]
      def self.expire_source_files(source_name, files)
        source_info = ::Friendlycontent::Rails.config.get_source_info(source_name)
        return false unless source_info

        virtual_paths = get_virtual_paths source_info, files
        expire source_info, virtual_paths
      end

      # virtual_paths - array of paths
      # example: ['home/index', ..]
      def self.expire(source_name_or_info, virtual_paths)
        if source_name_or_info.is_a? String
          source_info = ::Friendlycontent::Rails.config.get_source_info source_name_or_info
        else
          source_info = source_name_or_info
        end
        return nil unless source_info

        virtual_paths.each do |path|
          ::Friendlycontent::Rails::CacheManager.expire source_info, path
        end

        true
      end

      def self.download_files(source_name_or_info, virtual_paths)
        if source_name_or_info.is_a? String
          source_info = ::Friendlycontent::Rails.config.get_source_info source_name_or_info
        else
          source_info = source_name_or_info
        end
        return nil unless source_info

        virtual_paths.each do |path|
          download source_info, path
        end
      end

      def self.download(source_name_or_info, virtual_path, options={})
        if source_name_or_info.is_a? String
          source_info = ::Friendlycontent::Rails.config.get_source_info source_name_or_info
        else
          source_info = source_name_or_info
        end
        return nil unless source_info

        content_block = ::Friendlycontent::ContentBlock::Factory.create source_info, virtual_path, {}
        content_block.download
      end

      def self.get_virtual_paths(source_info, paths)
        paths.map{ |f| get_virtual_path(source_info, f)}
      end

      def self.get_virtual_path(source_info, path)
        fullpath = nil

        a = path.split /\./
        if a.count <=1
          fullpath = path
        elsif a.count == 2
          fullpath = a[0]
        else
          # remove last two
          fullpath = a[0..-3].join('.')
        end

        #
        source_path = source_info.get_property(:server_path, '')
        virtual_path = fullpath.gsub /^#{source_path}/, ''
        virtual_path.gsub! /^\//, ''
        virtual_path
      end
    end
  end
end
