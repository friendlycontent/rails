module Friendlycontent
  module Rails
    class Engine < ::Rails::Engine
      isolate_namespace Friendlycontent::Rails

      # for Rails 5
      config.enable_dependency_loading = false
      config.eager_load_paths += %W( #{Friendlycontent::Rails::Engine.root}/lib )

      config.watchable_dirs['lib'] = [:rb] if ::Rails.env.development?
      config.watchable_dirs['app/helpers'] = [:rb] if ::Rails.env.development?

      config.autoload_paths += Dir["#{Friendlycontent::Rails::Engine.root}/app/helpers/"]
      config.autoload_paths += Dir["#{Friendlycontent::Rails::Engine.root}/app/helpers/**/"]
      config.autoload_paths += Dir["#{Friendlycontent::Rails::Engine.root}/lib/"]
      config.autoload_paths += Dir["#{Friendlycontent::Rails::Engine.root}/lib/**/"]

      config.before_initialize do
        ActiveSupport.on_load :action_controller do
          ::ActionController::Base.helper Friendlycontent::Rails::Engine.helpers
        end
      end
    end
  end
end
