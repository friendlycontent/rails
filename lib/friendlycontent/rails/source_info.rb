module Friendlycontent
  module Rails
    class SourceInfo
      attr_accessor :name, :data

      def initialize(_name, _source_info)
        @name = _name
        @data = _source_info
      end

      def type
        @data[:type] || 'local'
      end

      def is_local?
        type=='local'
      end

      def get_source_class
        # build by source class
        prop_class_source = get_property :class_source
        return nil if prop_class_source.nil?

        cls_source = nil
        if prop_class_source.is_a?(String)
          cls_source = Object::const_get(prop_class_source)
        else
          cls_source = prop_class_source
        end

        cls_source
      end


      def get_local_path
        get_property(:local_path, 'app/views')
      end

      def get_property(prop_name, def_value=nil)
        @data[prop_name.to_sym] || def_value
      end

      def cache_ttl
        @data[:cache_ttl] || ::Friendlycontent::Rails.config.cache_ttl
      end
    end
  end
end
