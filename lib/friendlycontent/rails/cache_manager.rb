module Friendlycontent
  module Rails
    class CacheManager

      def self.expire(source_info, virtual_path, options={})
        content_block = ::Friendlycontent::ContentBlock::Factory.create source_info, virtual_path, options
        content_block.expire!
      end

    end
  end
end

