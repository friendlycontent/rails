require "friendlycontent/rails/engine"
require "friendlycontent/rails/configuration"

module Friendlycontent
  module Rails
    class << self
      attr_accessor :config
    end

    def self.config
      @config ||= Configuration.new
    end

    def self.reset
      @config = Configuration.new
    end

    def self.configure
      yield(config) if block_given?

    end
  end
end
