$:.push File.expand_path("lib", __dir__)

# Maintain your gem's version:
require "friendlycontent/rails/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |spec|
  spec.name        = "friendlycontent-rails"
  spec.version     = Friendlycontent::Rails::VERSION
  spec.authors     = ["Max Ivak"]
  spec.email       = ["maxivak@gmail.com"]
  spec.homepage    = "https://gitlab.com/friendlycontent/rails"
  spec.summary     = "Use remote content stored on github, gitlab in your Rails app"
  spec.description = "Render content from remote sources in Rails app"
  spec.license     = "MIT"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["allowed_push_host"] = 'http://mygemserver.com'
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  spec.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  spec.add_dependency "rails", ">= 5.1"
  spec.add_dependency "friendlycontent-source"

  spec.add_development_dependency "friendlycontent-source"
  spec.add_development_dependency "friendlycontent-source-cloud"
  spec.add_development_dependency "friendlycontent-source-github"
  spec.add_development_dependency "friendlycontent-source-gitlab"

end
